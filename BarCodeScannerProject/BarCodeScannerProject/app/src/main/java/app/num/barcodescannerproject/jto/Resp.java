package app.num.barcodescannerproject.jto;
import java.util.ArrayList;

public class Resp {
    Boolean success = null;
    String message = null;
    ArrayList<Data> dataList = new ArrayList();

    public Resp(){

    }

    public void addData (Data data) {
        this.dataList.add(data);
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDataList(ArrayList<Data> dataList) {
        this.dataList = dataList;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Data> getDataList() {
        return dataList;
    }
}
