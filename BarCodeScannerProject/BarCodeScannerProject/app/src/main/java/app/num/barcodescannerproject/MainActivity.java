package app.num.barcodescannerproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.zxing.Result;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import app.num.barcodescannerproject.dto.*;
import app.num.barcodescannerproject.jto.*;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static java.lang.String.format;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    private static Resp resp;
    private static Inventory userInventory;
    Example example = new Example();
    final ObjectMapper mapper = new ObjectMapper();
    TableLayout tabLay, tabLay2;
    TableRow tabRow;
    TextView label1, label2, label3;
    Button btnScan;
    boolean btnCl;
    int fio1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view();
    }

    public void view () {
//        mScannerView = null;
        setContentView(R.layout.activity_main);
//        btnScan = (Button) findViewById(R.id.buttonScan);
        tabLay = (TableLayout) findViewById(R.id.tableLayout);
        tabLay2 = (TableLayout) findViewById(R.id.tableLayout2);
            if (btnCl == true) {
                for (int i = 0; i < userInventory.getScanList().size(); i++) {
                    label1 = new TextView(this);
                    label2 = new TextView(this);
                    label3 = new TextView(this);
                    label1.setText(userInventory.getScanList().get(i).getItemId() + " ");
                    label2.setText(Boolean.toString(userInventory.getScanList().get(i).getItemRepair()) + " ");
                    label3.setText(Boolean.toString(userInventory.getScanList().get(i).getItemReplace()) + " ");
                    tabRow = new TableRow(this);
                    tabRow.setLayoutParams(new TableRow.LayoutParams(
                            TableRow.LayoutParams.MATCH_PARENT,
                            TableRow.LayoutParams.WRAP_CONTENT));
                    if (i % 2 == 0) tabRow.setBackgroundColor(0xffcccccc);
                    tabRow.addView(label1);
                    tabRow.addView(label2);
                    tabRow.addView(label3);
                    tabLay.addView(tabRow, new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.MATCH_PARENT,
                            204));
                    tabLay.setStretchAllColumns(true);
                }
            }
    }

    public void openBD(View view) throws IOException {
        Intent browserIntent = new Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://simple-inv.herokuapp.com/inventory"));
        startActivity(browserIntent);
    }

    public void onClickBD(View view) throws IOException {
        resp = new Resp();
        if (userInventory != null) {
            try {
                String jsonInString = mapper.writeValueAsString(userInventory);
                try {
                    String response = example.post("https://simple-inv.herokuapp.com/api/inventory", jsonInString);
                    Reader reader = new StringReader(response);
                    resp = mapper.readValue(reader, Resp.class);
                    for (int i = 0; i < resp.getDataList().size(); i++) {
                        label1 = new TextView(this);
                        label2 = new TextView(this);
                        label1.setText(resp.getDataList().get(0).getItemId() + " ");
                        label2.setText(resp.getDataList().get(0).getItemName() + " ");
                        tabRow = new TableRow(this);
                        tabRow.setLayoutParams(new TableRow.LayoutParams(
                                TableRow.LayoutParams.MATCH_PARENT,
                                TableRow.LayoutParams.WRAP_CONTENT));
                        if (i % 2 == 0) tabRow.setBackgroundColor(0xffcccccc);
                        tabRow.addView(label1);
                        tabRow.addView(label2);
                        tabLay2.addView(tabRow, new TableLayout.LayoutParams(
                                TableLayout.LayoutParams.MATCH_PARENT,
                                113));
                        tabLay2.setStretchAllColumns(true);
                    }
                    Toast.makeText(getApplicationContext(), resp.getMessage(), Toast.LENGTH_LONG)
                            .show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG)
                            .show();
                }
            } catch (JsonProcessingException e) {
                Toast.makeText(getApplicationContext(), "Ошибка формировки json'а", Toast.LENGTH_LONG)
                        .show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Проведите сканирование", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public void QrScanner(View view) {
        userInventory = new Inventory();
        btnCl = true;
        mScannerView = new ZXingScannerView(MainActivity.this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.setResultHandler(MainActivity.this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            mScannerView.stopCamera();           // Stop camera on pause
        }catch (Exception e) {
//            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG)
//                    .show();
        }
    }

    @Override
    public void handleResult(final Result rawResult) {
        final AlertDialog.Builder subDialog,mainDialog;

        final String[] sCheckName = { "Петров И.С.", "Сменов К.А.", "Орехов Г.Р." };
        subDialog =  new AlertDialog.Builder(MainActivity.this)
                .setTitle("ФИО")
                .setCancelable(false)
                .setItems(sCheckName, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        fio1 = which;
                        Date currentDate = new Date();  // Текущая дата
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd"); // Задаем формат даты
                        String formattedDate = sdf.format(currentDate); // и форматируем
                        userInventory.setDate(formattedDate);
                        userInventory.setFio(fio1 + 1);
                        mScannerView.stopCameraPreview();
                        mScannerView.stopCamera();
                        view();
                    }
                });

        final boolean[] mCheckedItems = { false,false };
        final String[] mCheckName = new String[]{ "Ремонт", "Замена" };
        mainDialog = new AlertDialog.Builder(this)
                .setTitle("Необходим:")
                .setCancelable(false)
                .setMultiChoiceItems(mCheckName, mCheckedItems,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which, boolean isChecked) {
                                mCheckedItems[which] = isChecked;
                            }
                        })
                .setPositiveButton("End process", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Scan newscan = new Scan();
                            newscan.setItemId(Integer.parseInt(rawResult.getText()));
                            newscan.setItemRepair(mCheckedItems[0]);
                            newscan.setItemReplace(mCheckedItems[1]);
                            userInventory.addScan(newscan);
                            dialog.cancel();
                            subDialog.show();
                        } catch(Exception e){
                            Toast.makeText(getApplicationContext(),
                                    "Просканирован неправильный QR-код: " + rawResult.getText(),
                                    Toast.LENGTH_LONG)
                                    .show();
                            dialog.cancel();
                            mScannerView.resumeCameraPreview(MainActivity.this);
                        }
                    }
                })
                .setNegativeButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Scan newscan = new Scan();
                            newscan.setItemId(Integer.parseInt(rawResult.getText()));
                            newscan.setItemRepair(mCheckedItems[0]);
                            newscan.setItemReplace(mCheckedItems[1]);
                            userInventory.addScan(newscan);
                            dialog.cancel();
                            mScannerView.resumeCameraPreview(MainActivity.this);
                        } catch(Exception e){
                            Toast.makeText(getApplicationContext(),
                                    "Просканирован неправильный QR-код: " + rawResult.getText(),
                                    Toast.LENGTH_LONG)
                                    .show();
                            dialog.cancel();
                            mScannerView.resumeCameraPreview(MainActivity.this);
                        }
                    }
                });
        mainDialog.show();
    }

    @Override
    public void onBackPressed() {
        openQuitDialog();
    }

    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(this)
        .setTitle("Выход: Вы уверены?")
        .setPositiveButton("Да", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                finish();
            }
        })
        .setNeutralButton("В меню", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                btnCl = false;
                mScannerView.stopCameraPreview();
                mScannerView.stopCamera();
                view();
            }
        })
        .setNegativeButton("Нет", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
            }
        });
        quitDialog.show();
    }
}
