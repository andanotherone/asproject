package app.num.barcodescannerproject.jto;

public class Data {
    String itemId = null;
    String itemName = null;

    public Data (){
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }
}
