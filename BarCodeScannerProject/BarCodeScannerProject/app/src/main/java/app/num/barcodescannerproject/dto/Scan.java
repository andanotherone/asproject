package app.num.barcodescannerproject.dto;
import java.lang.String;

public class Scan {
    int itemId;
    Boolean itemRepair = null;
    Boolean itemReplace = null;

    public Scan() {

    }



    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemId() {
        return this.itemId;
    }

    public void setItemRepair(boolean itemRepair) {
        this.itemRepair = itemRepair;
    }

    public boolean getItemRepair() {
        return this.itemRepair;
    }

    public void setItemReplace(boolean itemReplace) {
        this.itemReplace = itemReplace;
    }

    public boolean getItemReplace() {
        return this.itemReplace;
    }
}
