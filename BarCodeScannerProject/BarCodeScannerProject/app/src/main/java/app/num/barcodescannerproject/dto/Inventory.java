package app.num.barcodescannerproject.dto;
import java.util.ArrayList;
import java.lang.String;

public class Inventory {

    int fio;
    String date = null;
    ArrayList<Scan> scanList = new ArrayList();

    public Inventory() {

    }

    public void addScan(Scan scan) {
        this.scanList.add(scan);
    }

    public void setFio(int fio) {
        this.fio = fio;
    }

    public int getFio() {
        return this.fio;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return this.date;
    }

    public void setScanList(ArrayList<Scan> scanList) {
        this.scanList = scanList;
    }

    public ArrayList<Scan> getScanList() {
        return this.scanList;
    }
}
